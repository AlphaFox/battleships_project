from classes import *

# variables
auto_display = False
game = Game()

# welcome message
print('Welcome to battleships you can begin by placing ships,\nthe `place [letter] [number]` command will place a ship,\nfor more information type ? or help')
while True:
    # take an input from the user and parse the arguments into usable data types
    raw = input("Battleships> ").split()
    parsed = Parse(raw)

    # commands that can run at any point
    if parsed.cmd == 'ships':
        game.display_grid(ships=True)
    elif parsed.cmd == 'auto_display':
        auto_display = not auto_display

    # commands for managing ships before the game starts
    elif parsed.cmd == 'place' and not game.started:
        game.grids[0].place(Ship(parsed.x, parsed.y, parsed.ns), si=False)
    elif parsed.cmd == 'remove' and not game.started:
        game.grids[0].remove(parsed.x, parsed.y)
    elif parsed.cmd == 'rotate' and not game.started:
        game.grids[0].rotate(parsed.x, parsed.y)
    elif parsed.cmd == 'randomize' and not game.started:  # DO NOT ADD TO HELP - used for debugging
        game.random_ships(0)

    # commands for changing difficulty before the game starts
    elif parsed.cmd == 'difficulty' and not game.started:
        game.difficulty(max(1, min(parsed.x, 3)))
    elif parsed.cmd == 'ship_amount' and not game.started:
        game.ships_amount = parsed.x
    elif parsed.cmd == 'board_size' and not game.started:
        game.grid_size(parsed.x, parsed.y)

    # START GAME
    elif parsed.cmd == 'start' and not game.started:
        game.start()

    # commands for playing game
    elif parsed.cmd == 'attack' and game.started:
        game.take_turn(parsed.x, parsed.y)
    elif parsed.cmd == 'shots' and game.started:
        game.display_grid(1, False)

    # help, exit and error commands
    elif parsed.cmd == 'help' or parsed.cmd == '?':
        game.help(raw[1] if len(raw) > 1 else '')
    elif parsed.cmd == 'cheat':  # DO NOT ADD TO HELP - used for debugging
        game.display_grid(0, True)
        game.display_grid(0, False)
        print('----------------------------------')
        game.display_grid(1, True)
        game.display_grid(1, False)
    elif parsed.cmd == 'debug' and game.started:  # DO NOT ADD TO HELP - used for debugging
        game.debug(max(1, parsed.x))
    elif parsed.cmd == 'exit':
        break
    else:
        print('Invalid command, type help or ? for a list of available commands')

    if auto_display:
        game.display_grid(0, True)
        game.display_grid(1, False)




