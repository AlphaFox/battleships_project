from random import randint, getrandbits


class Ship:  # constructor for ship data
    pos = [(1, 0), (1, 1), (1, 2)]
    north_south = True
    hits = 0

    def __init__(self, x, y, ns=True):
        self.set_position(x, y, ns)

    def set_position(self, x, y, ns=True):
        self.pos = [(x + 1 - int(ns), y + int(ns)), (x, y), (x - (1 - int(ns)), y - int(ns))]
        self.north_south = ns
        return self

    def rotate(self):
        x, y = self.pos[1]
        self.set_position(x, y, not self.north_south)
        return self


class Grid:  # constructor for grid data
    def __init__(self, x=6, y=6):
        self.x, self.y = x, y
        self.ships = list()
        self.total_hits = 0
        self.grid = [[(None, 0) for _ in range(x)] for _ in range(y)]

    def place(self, ship, si=True):
        for (x, y) in ship.pos:
            if x < 0 or y < 0 or x >= self.x or y >= self.y:
                if not si:
                    print('failed to place ship, it is out of bounds')
                return False
            for s in self.ships:
                if (x, y) in s.pos:
                    if not si:
                        print('failed to place ship, it overlaps with another ship')
                    return False
        self.ships.append(ship)
        for (x, y) in ship.pos:
            self.grid[y][x] = (ship, 0)
        if not si:
            print('Ship placed, you have now placed %s ships' % (len(self.ships)))
        return True

    def get_ship(self, x, y):
        return self.grid[y][x][0]

    def remove(self, x, y):
        s = self.get_ship(x, y)
        if type(s) is Ship:
            self.ships.remove(s)
            for (x, y) in s.pos:
                self.grid[y][x] = (None, 0)
            return True
        return False

    def rotate(self, x, y):
        s = self.get_ship(x, y)
        if type(s) is Ship and (x, y) in s.pos:
            self.remove(x, y)
            if not self.place(s.rotate(), False):
                self.place(s)
            print('Rotated ship')
            return True
        return False

    def target(self, x, y):
        ship, hit = self.grid[y][x]
        if hit > 0:
            return False
        self.grid[y][x] = (ship, 1)
        if type(ship) is Ship:
            ship.hits += 1
            self.total_hits += 1
            return True
        return False

    def sides(self, x, y):
        nsew = [(0, 1), (0, -1), (1, 0), (-1, 0)]
        c = 0
        for a, b in nsew:
            if x + a >= self.x or x + a < 0 or y + b >= self.y or y + b < 0:
                c += 1
            else:
                s, h = self.grid[y + b][x + a]
                c += h
        return c


class Game:  # main class that controls the game
    grids = [Grid(), Grid()]
    started = False
    ended = False
    dif = 2
    ships_amount = 3

    def difficulty(self, n):
        self.dif = n
        print('Difficulty set to: ' + ['easy', 'medium', 'hard'][n - 1])

    def grid_size(self, x, y):
        if x < 5 or y < 5 or x > 24 or y > 24:
            print('invalid sizes for grid, please use a minimum of 5 and maximum of 24')
        else:
            self.grids = [Grid(x, y), Grid(x, y)]

    def start(self):
        if len(self.grids[0].ships) != self.ships_amount:
            print('You have placed the incorrect amount of ships, you should have %s, you currently have %s' % (self.ships_amount, len(self.grids[0].ships)))
        else:
            self.random_ships()
            self.started = True

    def display_grid(self, grid=0, ships=True):
        g = self.grids[grid]
        for y in range(g.y):
            print(str(g.y - 1 - y), end='| ')
            for x in range(g.x):
                s, h = g.grid[g.y - 1 - y][x]
                if ships:
                    print('.' if s is None else '#' if h == 0 else 'X', end=' ')
                else:
                    print('.' if h == 0 else 'X' if s is None else 'o', end=' ')
            print('|')
        print('¯|', end='')
        for x in range(g.x):
            print('¯' + 'ABCDEFGHIJKLMNOPQRSTUVWX'[x], end='')
        print('¯|')

    def random_ships(self, grid=1):
        g = self.grids[grid]
        while len(g.ships) < self.ships_amount:
            g.place(Ship(randint(0, g.x), randint(0, g.y), bool(getrandbits(1))))

    def take_turn(self, x, y):
        g = self.grids[1]
        if x < 0 or y < 0 or x >= g.x or y >= g.y:
            print('Error target position out of bounds')
            return
        if g.target(x, y):
            print('Its a hit')
            self.check_win()
        else:
            print('Its a miss')
        if self.ai_turn():
            print('One of your ships was hit')
            self.check_win()

    def check_win(self):
        if self.grids[0].total_hits == 3 * self.ships_amount:
            print('Congrats you win')
        elif self.grids[1].total_hits == 3 * self.ships_amount:
            print('Sorry you lose')

    def debug(self, n):
        result = ['miss', 'hit']
        for x in range(0, n):
            print(str(x) + ' ' + result[int(self.ai_turn(0))] + ' ' + result[int(self.ai_turn(1))])
        self.check_win()

    def ai_turn(self, grid=0):
        g = self.grids[grid]
        x, y = 3, 3
        while g.grid[y][x][1] > 0:  # find a semi random position to attack
            x, y = Util.randbounded(g.x, g.y, self.dif)

        if g.total_hits % 3 > 0:  # detects if you have hit but not sunk any ships
            for iy in range(g.y):
                for ix in range(g.x):  # iterate through the grid til finding a position that has been hit
                    s, h = g.grid[y][x]
                    if h > 0 and type(s) is Ship and g.sides(ix, iy) < 4:
                        if s.hits == 1:  # attempts to find the rest of the ship it just hit
                            x, y = Util.rand_snew(ix, iy, g.x, g.y)
                            while g.grid[y][x][1] > 0:
                                x, y = Util.rand_snew(ix, iy, g.x, g.y)

                        elif s.hits == 2:  # ok so maybe the ai cheats a little bit
                            for (px, py) in s.pos:
                                if g.grid[y][x][1] == 0:
                                    x, y = px, py
        return g.target(x, y)

    def help(self, arg=''):
        if arg == 'place':
            print('Places a ship at given coordinates:  PLACE [letter] [number]')
        elif arg == 'remove':
            print('Removes a ship at the given coordinates:  REMOVE [letter] [number]')
        elif arg == 'rotate':
            print('Rotates a ship at the given coordinates: ROTATE [letter] [number]')
        elif arg == 'difficulty':
            print('Gives a choice of difficulty level as 1 Easy, 2 Medium and 3 Hard')
        elif arg == 'ship_amount':
            print('Allows you to set the amount of ships to play the game with')
        elif arg == 'board_size':
            print('Allows you to set board size, minimum of 5 and maximum of 24')
        elif arg == 'start':
            print('Starts the game if you have placed all your ships')
        elif arg == 'attack':
            print('Allows you to make an attack at given coordinates: ATTACK [letter] [number]')
        elif arg == 'shots':
            print('Displays the grid of attacks you\'ve made at the opponent\'s ships')
        elif arg == 'ships':
            print('Displays the grid of your ships')
        elif arg == 'auto_display':
            print(' Automatically runs the `ships` and `shots` commands after your turn')
        elif arg == 'exit':
            print('Exits the game')
        else:
            if self.started:  # if game started
                print('Available commands: \n attack, shots')
            else:  # game not started used for setup
                print('Available commands: \n start, place, remove, rotate, board_size, ship_amount and difficulty')
                # general help
            print(' help, exit, auto_display, ships\nFor more information about a command enter: HELP [command]')


class Parse:  # constructor for input() data parsing
    def __init__(self, raw):
        self.cmd = raw[0].lower()
        self.x = Util.to_int(raw[1]) if len(raw) > 1 else 1
        self.y = Util.to_int(raw[2]) if len(raw) > 2 else 1
        if len(raw) > 2 and raw[2].upper() in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
            self.x, self.y = self.y, self.x
        self.ns = bool(raw[3]) if len(raw) > 3 and raw[3].lower() in '0truefalse1' else True


class Util:  # utilities
    @staticmethod
    def to_int(n):
        let = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        if n in '1021120131415161718192232425':
            return int(n)
        elif n[0].upper() in let:
            return let.index(n[0].upper())
        else:
            return 0

    @staticmethod
    def randbounded(a, b, n):
        x = randint(0, a - 1)
        y = x % n + n * randint(0, b // n - 1)
        return x % a, y % b

    @staticmethod
    def rand_snew(x, y, mx, my):
        a, b = [(0, 1), (0, -1), (1, 0), (-1, 0)][randint(0, 3)]
        while x + a >= mx or x + a < 0 or y + b >= my or y + b < 0:
            a, b = [(0, 1), (0, -1), (1, 0), (-1, 0)][randint(0, 3)]
        return x + a, y + b
