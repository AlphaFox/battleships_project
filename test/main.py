import cmd
from test.test import Game

class Battleships(cmd.Cmd):
    intro = 'Welcome to the battleships   Type help or ? to list commands.\n'
    prompt = 'Battleships> '
    file = None

    def do_ships(self, arg):
        """Print the current ship positions:  SHIPS"""
        for y in range(bs.sizeY):
            print(str(bs.sizeY - 1 - y) + '| ', end='')
            for x in range(bs.sizeX):
                print(str(bs.grid[bs.sizeY - 1 - y][x]) + ' ', end='')
            print('|')
        print('¯|', end='')
        for x in range(bs.sizeX):
            print('¯' + 'ABCDEFGHIJKLMNOPQRSTUVWX'[x], end='')
        print('¯|')

    def do_place(self, arg):
        """Place a ship:  PLACE"""
        bs.place_ship(*parse(arg))

    def do_remove(self, arg):
        """Remove a ship:  REMOVE"""
        bs.remove_ship(*parse(arg))

def parse(arg):
    """Convert a series of zero or more numbers to an argument tuple"""
    order = lambda a: [a[1], a[0]] + a[2:] if a[1].upper() in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' else a
    to_int = lambda x: int(x) if x in '0123456789' else 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.index(x.upper())
    return tuple(map(to_int, order(arg.split())))


if __name__ == '__main__':
    bs = Game(8, 8)
    Battleships().cmdloop()
