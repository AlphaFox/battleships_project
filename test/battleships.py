
class Game:
    def __init__(self, x, y):
        x = max(4,min(x,24))
        y = max(4,min(y,24))
        self.sizeX = x
        self.sizeY = y
        self.grid = [['.' for sx in range(x)] for sy in range(y)]
        self.positions = list()

    def place_ship(self, cx, cy, ns=1):
        if cx in range(1-ns, self.sizeX-1+ns) and cy in range(ns, self.sizeY-ns):
            pos = [None]*3
            pos[0] = [cy, cx]
            pos[1] = [cy+1*ns, cx+1*(1-ns)]
            pos[2] = [cy-1*ns, cx-1*(1-ns)]
            for p in pos:
                if self.grid[p[0]][p[1]] != '.':
                    print('Error: Ye be tryna launch ye ship into the bow of me other ship')
                    return
            for p in pos:
                self.grid[p[0]][p[1]] = '#'
            self.positions.append([cx, cy, ns])
        else:
            print('Error: Ye be tryna sail outside of known waters')

    def remove_ship(self, cx, cy):
        if cx in range(self.sizeX) and cy in range(self.sizeY):
            pos = [None]*3
            pos[0] = [cy, cx]
            pos[1] = [cy+1*ns, cx+1*(1-ns)]
            pos[2] = [cy-1*ns, cx-1*(1-ns)]
            for p in pos:
                if self.grid[p[0]][p[1]] != '.':
                    print('Error: ye be tryna launch ye ship into the bow of me other ship')
                    return
            for p in pos:
                self.grid[p[0]][p[1]] = '#'
                self.positions.append([cx, cy, ns])
        else:
            print('Error: argh ye won\'t find ships outside of known waters')

